﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class swipeRegMenu : MonoBehaviour
{
    private Vector2 fingerDownPos;
    private Vector2 fingerUpPos;
    public GameObject inf1, inf2, inf3, inf4,pin1, pin2, pin3, pin4, pin1B, pin2B, pin3B, pin4B;
    public int swipeCount = 0;

    public bool detectSwipeAfterRelease = true;

    public float SWIPE_THRESHOLD = 20f;

    // Update is called once per frame
    void Update()
    {

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUpPos = touch.position;
                fingerDownPos = touch.position;
            }

            //Detects Swipe while finger is still moving on screen
            if (touch.phase == TouchPhase.Moved)
            {
                if (!detectSwipeAfterRelease)
                {
                    fingerDownPos = touch.position;
                    DetectSwipe();
                }
            }

            //Detects swipe after finger is released from screen
            if (touch.phase == TouchPhase.Ended)
            {
                fingerDownPos = touch.position;
                DetectSwipe();
            }
        }
       /*if(Input.GetMouseButtonDown(0))
        {
            fingerDownPos = Input.mousePosition;
            DetectSwipe();
        }
       else if(Input.GetMouseButtonUp(0))
        {
            fingerUpPos = Input.mousePosition;
        }*/
    }
    
    void DetectSwipe()
    {

        if (VerticalMoveValue() > SWIPE_THRESHOLD && VerticalMoveValue() > HorizontalMoveValue())
        {
            Debug.Log("Vertical Swipe Detected!");
            if (fingerDownPos.y - fingerUpPos.y > 0)
            {
                OnSwipeUp();
            }
            else if (fingerDownPos.y - fingerUpPos.y < 0)
            {
                OnSwipeDown();
            }
            fingerUpPos = fingerDownPos;

        }
        else if (HorizontalMoveValue() > SWIPE_THRESHOLD && HorizontalMoveValue() > VerticalMoveValue())
        {
            Debug.Log("Horizontal Swipe Detected!");
            if (fingerDownPos.x - fingerUpPos.x > 0)
            {
                OnSwipeRight();
            }
            else if (fingerDownPos.x - fingerUpPos.x < 0)
            {
                OnSwipeLeft();
            }
            fingerUpPos = fingerDownPos;

        }
        else
        {
            Debug.Log("No Swipe Detected!");
        }
    }

    float VerticalMoveValue()
    {
        return Mathf.Abs(fingerDownPos.y - fingerUpPos.y);
    }

    float HorizontalMoveValue()
    {
        return Mathf.Abs(fingerDownPos.x - fingerUpPos.x);
    }

    void OnSwipeUp()
    {
        swipeCount++;
        swiped(swipeCount);
    }

    void OnSwipeDown()
    {
        swipeCount--;
        swiped(swipeCount);
    }

    void OnSwipeLeft()
    {
        swipeCount++;
        swiped(swipeCount);
    }

    void OnSwipeRight()
    {
        swipeCount--;
        swiped(swipeCount);
    }
    public void swiped(int id)
    {
        Debug.Log(id);
        if (id < 0)
        {
            id = 3;
            swipeCount = 3;
        }
        if (id > 3)
        {
            id = 0;
            swipeCount = 0;
        }
        Debug.Log(id);
        Debug.Log(swipeCount);
        if (id == 0)
        {
            inf1.SetActive(true);
            inf2.SetActive(false);
            inf3.SetActive(false);
            inf4.SetActive(false);

          
            pin1B.SetActive(true);
            pin4B.SetActive(false);
            pin2B.SetActive(false);


        }
        if (id == 1)
        {
            inf1.SetActive(false);
            inf2.SetActive(true);
            inf3.SetActive(false);
            inf4.SetActive(false);

           
            pin1B.SetActive(false);
            pin2B.SetActive(true);
            pin3B.SetActive(false);
        }
        if (id == 2)
        {
            inf1.SetActive(false);
            inf2.SetActive(false);
            inf3.SetActive(true);
            inf4.SetActive(false);

          
            pin2B.SetActive(false);
            pin3B.SetActive(true);
            pin4B.SetActive(false);

        }
        if (id == 3)
        {
            inf1.SetActive(false);
            inf2.SetActive(false);
            inf3.SetActive(false);
            inf4.SetActive(true);

            
            pin3B.SetActive(false);
            pin4B.SetActive(true);
            pin1B.SetActive(false);
        }
    }
}
