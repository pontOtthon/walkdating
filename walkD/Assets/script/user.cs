﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class user 
{
    public string username;
    public string coord;
    public string name;
    public string gender;
    public string dateOfLog;

    public user()
    {
    }

    public user(string name,string username, string coord,string gender)
    {
        this.username = username;
        this.coord = coord;
        this.name = name;
        this.gender = gender;
        dateOfLog = System.DateTime.Now.ToString();
    }
}
