﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using Facebook.MiniJSON;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using Firebase;
using Firebase.Storage;
using Firebase.Unity.Editor;
using System.Threading.Tasks;

public class facebbok : MonoBehaviour
{

    public static string id = "";
    public static string name = "";
    public static string gender = "";
    public static string gender_out = "";
    private AccessToken aToken;
    public static string kiiras;

    void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            FB.ActivateApp();
        }
        DontDestroyOnLoad(this.gameObject);
        Screen.SetResolution(720, 1280,true);
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }

        if (FB.IsLoggedIn)
        {
            /*aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            FB.API("/me?fields=first_name,gender", HttpMethod.GET, DispName);
            FB.API("/me/picture?type=square&height=1024&width=1024", HttpMethod.GET, GetPicture);
            id = aToken.UserId;
            localDatas.localme = true;
            StartCoroutine(loadScennes(1));*/
            //localDatas.kistring=localDatas.kistring + "\n its loged in";
        }
        else
        {
            Debug.Log("Please login to continue.");
           
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0; //pause
        }
        else
        {
            Time.timeScale = 1; //resume
        }
    }


    public void LoginWithFB()
    {
        var perms = new List<string>() { "public_profile","user_gender" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
        Debug.Log("LoginWhitFacebbok");
      
        
    }
    

    private void AuthCallback(ILoginResult result)
    {
        if (result.Error != null)
        {
            //localDatas.kistring=localDatas.kistring + "\n AuthCallback error";
        }
        if (FB.IsLoggedIn)
        {
            //localDatas.kistring=localDatas.kistring + "\n AuthCallback";
            aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            FB.API("/me?fields=first_name,gender", HttpMethod.GET, DispName);
            FB.API("/me/picture?type=square&height=1024&width=1024", HttpMethod.GET, GetPicture);
            id = aToken.UserId;
            localDatas.localme = true;
            StartCoroutine(loadScennes(1));

        }

    }

    void DispName(IResult result)
    {
        if (result.Error != null)
        {
            Debug.Log(result.Error);
        }
        else
        {
            name = result.ResultDictionary["first_name"].ToString();
            if (result.ResultDictionary.TryGetValue("gender", out gender_out))
            {
                gender = gender_out.ToString();
            }
            //gender = "male";
            Debug.Log("Hi there: " + result.ResultDictionary["first_name"]);
        }
    }

    public void GetPicture(IGraphResult result)
    {
      kiiras=kiiras + "\n" + Application.persistentDataPath + "/profile.png";
        if (result.Error == null)
        {
           
            byte[] bytes =result.Texture.EncodeToPNG();
          kiiras=kiiras + "\n" + Application.persistentDataPath + "/profile.png";
            File.WriteAllBytes(Application.persistentDataPath + "/profile.png", bytes);
            StartCoroutine(fileToWeb("file:///"+Application.persistentDataPath + "/profile.png"));
            
        }
        else
        {
          kiiras=kiiras + "\n" + result.Error+"error";
        }
    }

    IEnumerator fileToWeb(string path)
    {
        // Get a reference to the storage service, using the default Firebase App
        Firebase.Storage.FirebaseStorage storage = Firebase.Storage.FirebaseStorage.DefaultInstance;
        // Create a storage reference from our storage service
      
        Firebase.Storage.StorageReference storage_ref =
          storage.GetReferenceFromUrl("gs://walkdating-acae5.appspot.com/");
     
       // Create a reference to the file you want to upload
        Firebase.Storage.StorageReference profile_ref = storage_ref.Child("profiles/"+id+".png");
   
        
      var task = profile_ref.PutFileAsync("file://"+Application.persistentDataPath + "/profile.png");
      yield return new WaitUntil(() => task.IsCompleted); 
      if (task.IsFaulted || task.IsCanceled)
      {
        kiiras=kiiras + "\n" + task.Exception.ToString()+"hiba a storageba";
      kiiras=kiiras + "\n" + profile_ref.Path;
      kiiras=kiiras + "\n" + profile_ref.Name;
      kiiras=kiiras + "\n" + profile_ref.Storage;
          // Uh-oh, an error occurred!
      }
      else
      {
          // Metadata contains file metadata such as size, content-type, and download URL.
          Firebase.Storage.StorageMetadata metadata = task.Result;
          string download_url =profile_ref.GetDownloadUrlAsync().ToString();
           
        }
       

    }
    IEnumerator loadScennes(int scene)
    {
        yield return new WaitForSeconds(4);
        SceneManager.LoadScene(scene);
    }

}
/*  kiiras=kiiras + "\n" + "Finished uploading...";
                  kiiras=kiiras + "\n" + "download url = " + download_url;*/
/* FB.API("/me?fields=name", HttpMethod.GET, DispName);
FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, GetPicture);*/
/* public void LogoutFromFB()
 {
     FB.LogOut(); btnLi.SetActive(true); btnLo.SetActive(false);
 }*/
//yield return new WaitForSeconds(1);
/* WWWForm postForm = new WWWForm();
 postForm.AddBinaryData("theFile", localFile.bytes, id + "_profile.png");
 WWW upload = new WWW("http://ewulusen.uw.hu/upload.php", postForm);
 yield return upload;
 if (upload.error == null) { }
 else {
   kiiras=kiiras + "\n" + upload.error;
 }*/

