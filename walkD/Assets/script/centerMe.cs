﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class centerMe : MonoBehaviour
{
    public ScrollRect myScrollRect;
    public float SnapSpeed = 10;
    public int Snaps;
    private Scrollbar scroll;
    // Start is called before the first frame update
    void Start()
    {
        scroll = FindObjectOfType<Scrollbar>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void whatPosition()
    {
        
        scroll.value=Mathf.Lerp(scroll.value, Mathf.Round(scroll.value * (Snaps - 1)) / (Snaps - 1), SnapSpeed * Time.deltaTime);
        Debug.Log(myScrollRect.verticalNormalizedPosition.ToString());
    }
}
